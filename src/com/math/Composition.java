package com.math;

/**
 * Created by Sergei_Piianzin on 10/2/2016.
 */
public class Composition {
    private int power;
    private double[] constants;

    public Composition(double[] constants) {
        this.power = constants.length;
        this.constants = constants;
    }

    public double getResult(double argument) {
        double result = 1.0;
        for(int i=0; i < power; i++) {
            result *= argument - constants[i];
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i=0; i < power; ++i) {
            sb.append("(x - ").append(constants[i]).append(")");
        }
        return sb.toString();
    }
}
