package com.math;

public class Main {

    public static void main(String[] args) throws Exception {
        double[] x = {-0.50, -0.25, 0.00};
        double[] y = {0.72, 1.27, 1.20};
        Polynomial poly = new Polynomial(x, y);
        System.out.println(poly);
    }
}
