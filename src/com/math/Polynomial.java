package com.math;

/**
 * Created by Sergei_Piianzin on 10/2/2016.
 */
public class Polynomial {

    private Composition[] compositions;
    private Factor[] factors;

    public Polynomial(double[] x, double[] y) throws Exception {
        if (x.length != y.length) {
            throw new Exception("Sizes of points are not equal!");
        }

        int size = x.length;
        compositions = new Composition[size];
        factors = new Factor[size];

        for(int i=0; i < size; i++) {
            compositions[i] = new Composition(Utils.exceptElement(x, i));
            double resultComposition = new Composition(Utils.exceptElement(x, i)).getResult(x[i]);
            factors[i] = new Factor(resultComposition, y[i]);
        }
    }

    public double getResult(double x) {
        double result = 0.0;
        for (int i=0; i < compositions.length; ++i) {
            result += factors[i].getFactor() * compositions[i].getResult(x);
        }
        return result;
    }

    private class Factor {
        private double denominator;
        private double y;

        public Factor(double denominator, double y) {
            this.denominator = denominator;
            this.y = y;
        }

        public double getFactor() {
            return denominator * y;
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i=0; i < compositions.length; ++i) {
            sb.append(compositions[i]).append("factor: ").append(factors[i].getFactor()).append("\n");
        }
        return sb.toString();
    }
}
