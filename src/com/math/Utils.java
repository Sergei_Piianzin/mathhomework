package com.math;

/**
 * Created by Sergei_Piianzin on 10/2/2016.
 */
public class Utils {
    public static double[] exceptElement(double[] array, int index) throws Exception{
        if (array.length < index) {
            throw new Exception("Index should be in bounds [0, array size]");
        }

        double[] result = new double[array.length - 1];
        int k=0;
        for (int i=0; i < array.length; ++i) {
            if (i != index) {
                result[k++] = array[i];
            }
        }
        return result;
    }
}
